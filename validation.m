% ====== ====== EIM validation
clear

% --- Function to interpolate
g = @(x,mu) mu + mu.^2 .* x;

% --- Set of interest
mu = 1:4;
x  = 0:0.01:3;

% --- Total evaluation
[X,MU] = meshgrid(mu,x);
G = g(X,MU);

% --- EIM interpolation
[T, H, Ix, Imu, Err] = eim(G, 1e-6, 4);

% --- Validation
Igmu = G(Ix,:);
Ig   = eim_interpolate(Igmu, T, H);
disp( sprintf("EIM error : %5.1f", norm(G-Ig)) );

% ------ Display example
figure(1)
clf

subplot(1,2,1)
hold on
grid on

for i=1:length(mu)
  plot(x, G(:,i), 'linewidth', 1.7)
end

xlabel("x")
ylabel("g(x,mu)")

l = legend("mu=1", "mu=2", "mu=3", "mu=4");
set(l, 'location', 'northwest', 'fontsize', 14);
set(gca, 'fontsize', 14)

subplot(1,2,2)
hold on
grid on
mu_test = 1;

plot(x, G(:,mu_test), 'linewidth', 1.7)
alp = T\Igmu(:,mu_test);

plot(x, H(:,1).*alp(1), 'linewidth', 1.7)
plot(x, H(:,2).*alp(2), 'linewidth', 1.7)
plot(x(Ix), G(Ix,mu_test), "*", 'markersize', 8, 'linewidth', 1.7)
xlabel("x")
ylabel("g(x,mu)")

l = legend("g(.,mu=1)", "h1", "h2", "IP");
set(l, 'location', 'northwest', 'fontsize', 14);
set(gca, 'fontsize', 14)


% ====== ====== GEIM validation

% --- GEIM interpolation
L = rand(length(x));
[T, H, Il, Imu, Err] = geim(G, L, 1e-6, 4);

% --- Validation
Igmu = L(Il,:)*G;
Ig   = eim_interpolate(Igmu, T, H);
disp( sprintf("GEIM error : %5.1f", norm(G-Ig)) );