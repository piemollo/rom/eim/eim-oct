%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [T, H, Ix, Imu, Err] = eim(G, eps, kmax, Xh, subx)
%
% This function computes the basis of the empirical interpolation method.
%
%  input: - G: data matrix to interpolate
%         - eps: threshold
%         - kmax: iteration limit, also limit the size of the output basis
%         - Xh: inner product matrix (optional)
%         - subx: sub-index list, Ix are only chosen in this list
%
% output: - T: interpolation matrix
%         - H: reduced basis
%         - Ix: index of space interpolation points
%         - Imu: index of parameter interpolation points
%         - Err: recorded error
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [T, H, Ix, Imu, Err] = eim(G, eps, kmax, Xh, subx)
	
  % --- Get sizes
  [N,K] = size(G);
  
  % --- Check Xh
  if exist('Xh','var')==0
    Xh = speye(N);
  end
  
  % --- Check subx
  if exist('subx','var')==0
    subx = 1:N;
  end
  
  % ------ Initiate structure
  Imu = zeros(kmax,1);
  Ix  = zeros(kmax,1);
  Err = zeros(kmax,1);
  T = [1];
  H = zeros(N,kmax);
  
	% ------ Initial iteration
	% --- mu1
  GXh = G'*Xh;
  normG = zeros(K,1);
  for q=1:K
    normG(q) = sqrt(GXh(q,:)*G(:,q));
  end
	[Err(1), Imu(1)] = max(normG);

	% --- x1
	[~, Ix(1)] = max(abs(G(:,Imu(1))));
  
  % --- h1
	H(:,1) = G(:, Imu(1)) ./ G(Ix(1), Imu(1));
	
	
	% ------ Loop
  k = 1;
  err = eps+1;
  
	while err>eps && k<kmax
    % --- Interpolation
    Ig = eim_interpolate(G(Ix(1:k),:), T, H(:,1:k));
    Dg = G - Ig;
    
    % --- compute err
    GXh = Dg'*Xh;
    for q=1:K
      normG(q) = sqrt(GXh(q,:)*Dg(:,q));
    end
    
    % --- Shortcut
    err = max(normG);
    if err > eps
      k = k+1;
      
      % --- New mu
      [Err(k), Imu(k)] = max(normG);
      
      % --- New x
      Igmu       = eim_interpolate(G(Ix(1:k-1),Imu(k)), T, H(:,1:k-1));
      [~, Ix(k)] = max( abs( G(:,Imu(k)) - Igmu ) );
      
      % --- New H
      H(:,k) = ( G(:, Imu(k)) - Igmu ) ./ ( G(Ix(k), Imu(k)) - Igmu(Ix(k)) );
      
      % --- New M
      T = [T, zeros(k-1,1); H(Ix(k),1:k-1), 1];
    end
	end
  
  % ------ Resizing
  Imu = Imu(1:k,1);
  Ix  =  Ix(1:k,1);
  Err = Err(1:k,1);
  H   = H(:,1:k);