%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [T, H, Ix, Imu, Err] = geim(G, L, eps, kmax, Xh, subx)
%
% This function computes the basis of the empirical interpolation method.
%
%  input: - G: data matrix to interpolate
%         - L: measurements matrix
%         - eps: threshold
%         - kmax: iteration limit, also limit the size of the output basis
%         - Xh: inner product matrix (optional)
%         - subx: sub-index list, Ix are only chosen in this list
%
% output: - T: interpolation matrix
%         - H: reduced basis
%         - Il: index of space interpolation forms
%         - Imu: index of parameter interpolation points
%         - Err: recorded error
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [T, H, Il, Imu, Err] = geim(G, L, eps, kmax, Xh, subx)
	
  % --- Get sizes
  [N,K] = size(G);
  
  % --- Check Xh
  if exist('Xh','var')==0
    Xh = speye(N);
  end
  
  % --- Check subx
  if exist('subx','var')==0
    subx = 1:N;
  end
  
  % ------ Initiate structure
  Imu = zeros(kmax,1);
  Il  = zeros(kmax,1);
  Err = zeros(kmax,1);
  T = [1];
  H = zeros(N,kmax);
  
	% ------ Initial iteration
	% --- mu1
  GXh = G'*Xh;
  normG = zeros(K,1);
  for q=1:K
    normG(q) = sqrt(GXh(q,:)*G(:,q));
  end
	[Err(1), Imu(1)] = max(normG);

	% --- x1
	[~, Il(1)] = max(abs(L*G(:,Imu(1))));
  
  % --- h1
	H(:,1) = G(:, Imu(1)) ./ ( L(Il(1),:)*G(:, Imu(1)) );
	
	
	% ------ Loop
  k = 1;
  err = eps+1;
  
	while err>eps && k<kmax
    % --- Interpolation
    Ig = eim_interpolate( L(Il(1:k),:)*G, T, H(:,1:k) );
    Dg = G - Ig;
    
    % --- compute err
    GXh = Dg'*Xh;
    for q=1:K
      normG(q) = sqrt(GXh(q,:)*Dg(:,q));
    end
    
    % --- Shortcut
    err = max(normG);
    if err > eps
      k = k+1;
      
      % --- New mu
      [Err(k), Imu(k)] = max(normG);
      
      % --- New x
      Igmu       = eim_interpolate( L(Il(1:k-1),:)*G(:,Imu(k)), T, H(:,1:k-1) );
      [~, Il(k)] = max( abs( L*( G(:,Imu(k)) - Igmu ) ) );
      
      % --- New H
      tmp = G(:, Imu(k)) - Igmu;
      if abs(L(Il(k),:)*tmp) <= 1e-15
        disp(sprintf("/!/ Warning: measurements low %5.2e iter:%d ", ...
          abs(L(Il(k),:)*tmp), k));
      end
      H(:,k) = tmp ./ ( L(Il(k),:)*tmp );
      
      % --- New M
      T = [T, zeros(k-1,1); L(Il(k),:)*H(:,1:k-1), 1];
    end
	end
  
  % ------ Resizing
  Imu = Imu(1:k,1);
  Il  =  Il(1:k,1);
  Err = Err(1:k,1);
  H   = H(:,1:k);